from django.urls import path
from receipts.views import (
    account_list,
    home,
    category_list,
    create_category,
    create_receipt,
    create_account,
)


urlpatterns = [
    path("", home, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("accounts/create/", create_account, name="create_account"),
    path("categories/create/", create_category, name="create_category")
]
